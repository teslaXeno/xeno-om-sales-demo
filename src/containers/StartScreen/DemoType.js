import React from "react";
import { Grid, Button, Typography, Box, MenuItem, Select } from "@mui/material";
import { industries } from "../../data/sales-data";

import useCartStore from "../../data/cartStore";
import { useNavigate } from "react-router-dom";
// this file will essentially enable the routing of demo
// the industry type decided will also fetch me the various products that will be there in my firestore

const DemoType = () => {
  const navigate = useNavigate();
  function handleDemoStart() {
    navigate('/pos/menu');
  }
  const setIndustry = useCartStore((state) => state.setIndustry);
  return (
    <>
      <Grid
        direction="column"
        container
        alignItems="center"
        spacing={5}
        justifyContent="center"
        style={{ minHeight: "100vh" }}
      >
        <Grid item xs={12}>
          <Typography
            variant="h4"
            sx={{
              textTransform: "capitalize",
            }}
            align="center"
          >
            Choose demo type
          </Typography>
        </Grid>
        <Grid
          container
          alignItems={"center"}
          justifyContent="center"
          spacing={4}
          item
          xs={12}
        >
          <Grid item xs={2}>
            {" "}
            <Typography>Checkout type</Typography>
          </Grid>
          <Grid item>
            <Select
              sx={{
                minWidth: 300,
              }}
            >
              <MenuItem value={"pos"}>POS</MenuItem>
              <MenuItem value={"ecommerce"}>E Commerce</MenuItem>
            </Select>
          </Grid>
        </Grid>
        <Grid
          container
          alignItems={"center"}
          justifyContent="center"
          spacing={4}
          item
          xs={12}
        >
          <Grid item xs={2}>
            {" "}
            <Typography>Industry</Typography>
          </Grid>
          <Grid item>
            <Select
              sx={{
                minWidth: 300,
              }}
              onChange={(event) => {
                setIndustry(event.target.value);
              }}
            >
              {industries.map((el) => (
                <MenuItem value={el.value}>{el.label}</MenuItem>
              ))}
            </Select>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Button
            onClick={handleDemoStart}
            fullWidth
            variant="contained"
            color="primary"
          >
            Start Demo
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default DemoType;
