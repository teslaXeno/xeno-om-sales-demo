import React from "react";
import NavBar from "../../components/Navbar";
import { Routes, Route, Outlet } from "react-router-dom";
import BillingMenu from "./BillingMenu";
import { Box } from "@mui/material";

const POSDemo = () => {
  return (
    <>
      {/* <BillingMenu /> */}
      <NavBar />
      <Box
        sx={{
          mt: "100px",
          height: "85vh",
        }}
      >
        <Outlet />
        {/* ? what the fuck is outlet */}
      </Box>
    </>
  );
};

export default POSDemo;
