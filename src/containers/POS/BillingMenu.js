import React from "react";
import NavBar from "../../components/Navbar";
import { Grid, MenuItem, Typography } from "@mui/material";
import MenuCheckoutSection from "../../components/MenuCheckoutSection";
import MenuItems from "../../components/MenuItems";

const BillingMenu = () => {
  return (
    <Grid height={"100%"} container spacing={1}  columnSpacing={2} mt={2}>
      <Grid
        sx={{
          border: "1px solid #efefef",
        }}
        item
        p={2}
        xs={7}
      >
        <MenuItems />
      </Grid>
      <Grid
        sx={{
          border: "1px solid #efefef",
        }}
        item
        p={2}
        xs={5}
      >
        <MenuCheckoutSection />
      </Grid>
    </Grid>
  );
};

export default BillingMenu;
