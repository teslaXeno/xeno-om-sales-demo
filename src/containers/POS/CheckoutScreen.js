import React from "react";
import { Grid, Typography, Box, Button, Divider } from "@mui/material";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from "@mui/material";
import useCartStore from "../../data/cartStore";
import { useNavigate } from "react-router-dom";
import { round } from "lodash";
import { generateBillNumber } from "../../data/sales-data";
export default function CheckoutScreen() {
  const items = useCartStore((state) => state.items);
  const merchantCurrency = useCartStore((state) => state.merchantCurrency);
  const total = useCartStore((state) => state.total);
  const subTotal = useCartStore((state) => state.subTotal);
  const totalDiscount = useCartStore((state) => state.totalDiscount);
  const redeemCoupon = useCartStore((state) => state.redeemCoupon);
  const offerDiscount = useCartStore((state) => state.offerDiscount);
  const couponCode = useCartStore((state) => state.couponCode);
  const navigate = useNavigate();
  return (
    <Grid container spacing={2}>
      <Grid item xs={6}>
        <Typography my={2} variant="h5" align="center">
          Cart Summary
        </Typography>
        <TableContainer component={Paper}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>No </TableCell>
                <TableCell>Product Name</TableCell>
                <TableCell align="right">Quantity</TableCell>
                <TableCell align="right">Price {merchantCurrency}</TableCell>
                <TableCell align="right">
                  Total Price {merchantCurrency}
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {Object.keys(items).map((key, index) => {
                const row = items[key];
                return (
                  <TableRow key={row.id}>
                    <TableCell component="th" scope="row">
                      {index + 1}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {row.Name}
                    </TableCell>
                    <TableCell align="right">{row.quantity}</TableCell>
                    <TableCell align="right">{row.Price}</TableCell>
                    <TableCell align="right">
                      {row.quantity * row.Price}
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <Grid container my={2} spacing={4} justifyContent={"space-between"}>
          <Grid item>
            <Typography my={1}>Sub Total:</Typography>
            <Typography my={1} variant="body1">
              Discount:
            </Typography>
            <Typography my={1}>Offer Discount: </Typography>
            <Typography color={"primary"} my={1}>
              Coupon Applied:{" "}
            </Typography>
          </Grid>
          <Grid item>
            <Typography my={1}>{round(total, 2)}</Typography>
            <Typography my={1} variant="body1">
              {round(totalDiscount, 2)}
            </Typography>
            <Typography my={1}> {offerDiscount} &nbsp;</Typography>
            <Typography color={"primary"} my={1}>
              {couponCode}
            </Typography>
          </Grid>
        </Grid>
        <Grid my={2} container justifyContent={"space-between"}>
          <Grid item>
            <Typography variant="h5" color={"primary"}>
              Total
            </Typography>
          </Grid>
          <Grid item>
            <Typography>
              {merchantCurrency} {round(subTotal, 2)}
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={6}>
        <Typography my={2} variant="h5" align="center">
          Payment Confirmation
        </Typography>
        <Paper style={{ padding: "16px", marginBottom: "16px" }}>
          <Typography variant="h6" gutterBottom>
            Bill Number: {generateBillNumber()}
          </Typography>
          <Typography variant="body1">Date: </Typography>
          <Divider />
          <Typography>Customer Info</Typography>
          <Typography>Paritosh Kumar</Typography>
          <Typography>+91-9589267126</Typography>
          <Divider />
          <Typography>Payment Method</Typography>
          <Grid container justifyContent={"center"} spacing={2}>
            <Grid item>
              <Button onClick={() => navigate("/pos/menu")} variant="outlined">
                Cancel
              </Button>
            </Grid>
            <Grid item>
              <Button
                variant="contained"
                color="primary"
                onClick={redeemCoupon}
              >
                Complete Payment
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  );
}
