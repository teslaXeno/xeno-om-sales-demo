export const industries = [
  { label: "Food and beverages", value: "foodAndBeverages" },
  { label: "Apparel", value: "apparel" },
  { label: "Cosmetics", value: "cosmetics" },
];

export function generateBillNumber() {
  const randomNumber = Math.floor(Math.random() * 10000); // Generates a random number between 0 and 9999999999
  const billNumber = "BILL-" + String(randomNumber).padStart(4, "0"); // Pads with zeros to ensure 10 digits
  return billNumber;
}
