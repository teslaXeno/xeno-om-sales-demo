import create from "zustand";
import { devtools } from "zustand/middleware";
import { produce } from "immer";
import { get as _get, omit, set as _set } from "lodash";

import axios from "axios";

// Define the cart's initial state
const initialState = {
  items: {},
  total: 0, // in cart terms this means sub-total
  industry: "apparel",
  selectedCategories: [],
  searchTerm: "",
  merchantType: "domestic",
  merchantCurrency: "INR",
  customerPhone: "9873052528",
  couponCode: "GTC2",
  referenceId: null,
  applyingCoupon: false,
  couponBlocked: false,
  totalDiscount: 0,
  billNo: null,
  subTotal: 0,
  finalTotal: 0, // this will be calculated after discount + offer benefit
  redeemingCoupon: false,
  transactionComplete: false,
  loadingProducts: false,
};
const immerMiddleware = (config) => (set, get, api) =>
  config((fn) => set(produce(fn)), get, api);

// Define the cart's actions
const cartActions = (set, get) => ({
  updateStore: (key, value) =>
    set((state) => {
      state[key] = value;
    }),
  addItem: (item) =>
    set((state) => {
      if (state.items[item.id]) state.items[item.id].quantity += 1;
      else state.items[item.id] = { ...item, quantity: 1 };
      state.total += item.Price;
      state.subTotal += (item.Price * (100 - item.Discount)) / 100;
      state.totalDiscount += (item.Price * item.Discount) / 100;
    }),
  deleteItem: (item) =>
    set((state) => {
      state.items = omit(state.items, [item.id]);
      state.total -= item.Price * item.quantity;
      state.subTotal -=
        (item.Price * item.quantity * (100 - item.Discount)) / 100;
      state.totalDiscount -= (item.Price * item.quantity * item.Discount) / 100;
    }),
  removeItem: (item) =>
    set((state) => {
      if (state.items[item.id].quantity === 1)
        state.items = omit(state.items, [item.id]);
      else state.items[item.id].quantity -= 1;
      state.total -= item.Price;
      state.subTotal -= (item.Price * (100 - item.Discount)) / 100;
      state.totalDiscount -= (item.Price * item.Discount) / 100;
    }),
  setCoupon: (couponCode) =>
    set((state) => {
      state.couponCode = couponCode;
    }),
  clearCart: () =>
    set((state) => {
      state.items = {};
      state.total = 0;
    }),
  setIndustry: (value) =>
    set((state) => {
      state.industry = value;
    }),
  updateSearchTerm: (value) =>
    set((state) => {
      state.searchTerm = value;
    }),
  updateSelectedCategories: (array) =>
    set((state) => {
      state.selectedCategories = array;
    }),

  redeemCoupon: async () => {
    let data = JSON.stringify({
      couponCode: get().couponCode,
      customerPhone: get().customerPhone,
      referenceId: get().referenceId,
      billDetails: {
        billNo: get().billNo,
        totalAmount: get().finalTotal,
        billTimestamp: "2023-08-18T15:57:00.000", // when bill was generated
        store: "alpha-1",
        channel: "online",
        currencyCode: get().merchantCurrency,
      },
      redemptionDateTime: "2023-07-18T15:57:00.000",
      discountAvailed: get().totalDiscount,
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: "https://offermanagement.dev.xeno.in/api/v1/coupon/redeem",
      headers: {
        "X-Request-Source": "callerInfo",
        Authorization: "Bearer zsdbha795t",
        "Content-Type": "application/json",
      },
      data: data,
    };
    set((state) => {
      state.redeemingCoupon = true;
    });
    axios
      .request(config)
      .then((response) => {
        set((state) => {
          state.redeemingCoupon = false;
          state.transactionComplete = true;
        });
        console.log(JSON.stringify(response.data));
      })
      .catch((error) => {
        console.log(error);
      });
  },

  blockCoupon: async () => {
    set((state) => {
      state.applyingCoupon = true;
    });
    let data = JSON.stringify({
      customerPhone: get().customerPhone,
      referenceId: get().referenceId,
      couponCode: get().couponCode,
      transactionDetails: {
        billNo: 123,
        totalAmount: get().total,
        currencyCode: get().merchantCurrency,
        currentTimestamp: new Date().toISOString(),
        store: "alpha-1",
        channel: "online",
        cartItems: [
          {
            skuId: 2,
            categoryId: "1",
            categoryName: null,
            productId: null,
            productName: null,
            departmentId: "1",
            departmentName: null,
            subCategoryId: "AC",
            subCategoryName: null,
            productQuantity: 1,
            productPrice: 1000,
            productDiscount: 20,
          },
        ],
      },
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: "https://offermanagement.dev.xeno.in/api/v1/coupon/validate?block=true",
      headers: {
        "X-Request-Source": "callerInfo",
        Authorization: "Bearer zsdbha795t",
        "Content-Type": "application/json",
      },
      data: data,
    };

    axios
      .request(config)
      .then((response) => {
        set((state) => {
          state.applyingCoupon = false;
          state.referenceId = _get(response, "data.data.referenceId");
          state.totalDiscount = _get(
            response,
            "data.data.couponBenefits.totalDiscount"
          );

          state.couponBlocked = true;
        });

        console.log(JSON.stringify(response.data));
      })
      .catch((error) => {
        set((state) => {
          state.applyingCoupon = false;
        });
        console.log(error);
      });
  },
  //   clearCart: () => set(() => initialState),
});

// Use Immer to handle state immutability

// Create the store
const useCartStore = create(
  devtools(
    immerMiddleware((set, get) => ({
      ...initialState,
      ...cartActions(set, get),
    })),
    "cart-store"
  )
);

export default useCartStore;
