import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAKDQjpGA8MNvxGO4sEczLE4wVmRn6lxT8",
  authDomain: "xeno-om.firebaseapp.com",
  projectId: "xeno-om",
  storageBucket: "xeno-om.appspot.com",
  messagingSenderId: "472196838862",
  appId: "1:472196838862:web:4058a98cc9f3ba6963a79f",
  measurementId: "G-XQ7PKPY5RH",
};

const app = initializeApp(firebaseConfig);

const db = getFirestore(app);

export default db;
