import { createTheme } from "@mui/material/styles";

const theme = createTheme({
  palette: {
    primary: {
      main: "#0f62fe",
    },
    default: {
      main: "#19857b",
    },
    // error: {
    //   main: "#ff3333",
    // },
    // background: {
    //   default: "#fff",
    // },
  },
});

export default theme;
