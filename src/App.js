import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
// import { CssBaseline } from "@material-ui/core";
import DemoType from "./containers/StartScreen/DemoType";
import { ThemeProvider } from "@mui/material";
import theme from "./styles/globalTheme";
import POSDemo from "./containers/POS";
import BillingMenu from "./containers/POS/BillingMenu";
import { Box } from "@mui/material";
import CartItem from "./components/CartItem";
import CheckoutScreen from "./containers/POS/CheckoutScreen";

function About() {
  return <div>About</div>;
}

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Box
          sx={{
            ml: 5,
            mr: 5,
            minHeight: "85vh",
          }}
        >
          {/* <CssBaseline /> */}
          <Routes>
            <Route exact path="/" element={<DemoType />} />
            <Route path="/pos" element={<POSDemo />}>
              <Route path="menu" element={<BillingMenu />} />
              <Route path="checkout" element={<CheckoutScreen />} />
            </Route>
            <Route path="/about" element={<About />} />
            <Route path="/cart-item" element={<CartItem />} />
          </Routes>
        </Box>
      </Router>
    </ThemeProvider>
  );
}

export default App;
