import { Typography, Grid, Box } from "@mui/material";
import React, { useState, useEffect } from "react";
import CartItem from "./CartItem";
import SearchBar from "./SearchBar";
import useCartStore from "../data/cartStore";
import db from "../firebase";
import {
  getFirestore,
  collection,
  query,
  where,
  getDocs,
  getDoc,
  doc,
} from "firebase/firestore";
import CircularProgress from "@mui/material/CircularProgress";

export default function MenuItems() {
  const [products, setProducts] = useState([]);
  const selectedCategories = useCartStore((state) => state.selectedCategories);
  const searchTerm = useCartStore((state) => state.searchTerm);
  const updateStore = useCartStore((state) => state.updateStore);
  const loadingProducts = useCartStore((state) => state.loadingProducts);
  useEffect(() => {
    const fetchProducts = async () => {
      updateStore("loadingProducts", true);
      const fetchedProducts = [];

      for (const category of selectedCategories) {
        const categoryRef = doc(db, "category", category); // Assuming category names are unique and can be used as document IDs

        const q = query(
          collection(db, "products"),
          where("Category", "==", categoryRef)
          // where("Name", ">=", searchTerm),
          // where("Name", "<=", searchTerm + "\uf8ff")
        );

        const querySnapshot = await getDocs(q);

        for (const productDoc of querySnapshot.docs) {
          const productData = productDoc.data();
          const categoryDoc = await getDoc(productData.Category);
          fetchedProducts.push({
            id: productDoc.id,
            ...productData,
            categoryName: categoryDoc.data().Name,
          });
        }
      }

      setProducts(fetchedProducts);
      updateStore("loadingProducts", false);
    };

    fetchProducts();
  }, [selectedCategories]);

  const cartItems = Array(20)
    .fill()
    .map((_, idx) => (
      <Grid item xs={4}>
        <CartItem />
      </Grid>
    ));
  return (
    <Box>
      <Box pb={2}>
        <SearchBar />
      </Box>
      <Grid
        container
        rowSpacing={2}
        spacing={2}
        sx={{
          height: "60vh",
          overflowY: "auto",
        }}
      >
        <Grid item xs={12} sx={{
          textAlign:"center"
        }}>
          {loadingProducts && <CircularProgress size={24} />}
        </Grid>
        {products
          .filter((el) =>
            el.Name.toLowerCase().includes(searchTerm.toLowerCase())
          )
          .map((product) => (
            <Grid item xs={4}>
              {" "}
              <CartItem {...product} />
            </Grid>
          ))}
      </Grid>
    </Box>
  );
}
