import React from "react";
import {
  Card,
  CardContent,
  CardMedia,
  Chip,
  Typography,
  Button,
  Grid,
} from "@mui/material";
import useCartStore from "../data/cartStore";
import { get } from "lodash";

function CartItem(item) {
  const { Name, Price, categoryName } = item;
  const addItem = useCartStore((state) => state.addItem);
  const items = useCartStore((state) => state.items);
  const merchantCurrency = useCartStore((state) => state.merchantCurrency);
  function addItemToCart() {
    addItem(item);
  }
  return (
    <Card sx={{ maxWidth: 250, position: "relative" }}>
      {/* Image placeholder with Chip */}

      {/* <CardMedia
        component="img"
        height="120"
        image="https://via.placeholder.com/345x140" // Replace this with your image URL
        alt="Image placeholder"
      /> */}
      {get(items, `[${item.id}].quantity`, 0) > 0 && (
        <>
          <Chip
            label={get(items, `[${item.id}].quantity`, 0)}
            color="primary"
            sx={{ position: "absolute", top: 10, right: 10 }}
          />
        </>
      )}

      {/* Content */}
      <CardContent>
        <Grid container alignItems={"center"} justifyContent={"space-between"}>
          <Grid item xs={12}>
            <Typography gutterBottom variant="h5" component="div">
              {Name}
            </Typography>
          </Grid>
          <Grid item>
            <Typography
              gutterBottom
              variant="h6"
              component="div"
              color="primary"
            >
              {merchantCurrency} {Price}
            </Typography>
          </Grid>
        </Grid>

        <Typography variant="body2" color="textSecondary">
          {categoryName}
        </Typography>
        <Button
          onClick={addItemToCart}
          variant="contained"
          fullWidth
          color="primary"
          sx={{ mt: 2 }}
        >
          Add to Cart
        </Button>
      </CardContent>
    </Card>
  );
}

export default CartItem;
