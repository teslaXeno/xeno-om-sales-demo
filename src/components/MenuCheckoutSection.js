import React from "react";
import {
  Box,
  Typography,
  TextField,
  Button,
  IconButton,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  Grid,
  Divider,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import OffersSection from "./OffersSection";
import InputAdornment from "@mui/material/InputAdornment";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import { isEmpty, round } from "lodash";
import useCartStore from "../data/cartStore";
import DeleteIcon from "@mui/icons-material/Delete";

import CloseIcon from "@mui/icons-material/Close";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import CircularProgress from "@mui/material/CircularProgress";

function MenuCheckoutSection() {
  const navigate = useNavigate();
  const items = useCartStore((state) => state.items);
  const applyingCoupon = useCartStore((state) => state.applyingCoupon);
  const merchantCurrency = useCartStore((state) => state.merchantCurrency);
  const total = useCartStore((state) => state.total);
  const removeItem = useCartStore((state) => state.removeItem);
  const addItem = useCartStore((state) => state.addItem);
  const deleteItem = useCartStore((state) => state.deleteItem);
  const clearCart = useCartStore((state) => state.clearCart);
  const blockCoupon = useCartStore((state) => state.blockCoupon);
  const couponCode = useCartStore((state) => state.couponCode);
  const couponBlocked = useCartStore((state) => state.couponBlocked);
  const updateStore = useCartStore((state) => state.updateStore);
  const subTotal = useCartStore((state) => state.subTotal);
  // Sample data and structure for demonstration. In a real-world app, this would likely come from state or an API.

  function handleManualCouponInput(event) {
    updateStore("couponCode", event.target.value);
    updateStore("couponBlocked", false);
  }

  return (
    <Box
      sx={{ width: "100%", p: 2, bgcolor: "background.paper", borderRadius: 2 }}
    >
      {/* Order Info */}
      <Grid
        container
        alignItems={"center"}
        mb={2}
        justifyContent={"space-between"}
      >
        <Grid item>
          <Typography variant="h6">
            Order ID:{" "}
            <Typography component={"span"} sx={{ opacity: 0.7 }}>
              # 123456
            </Typography>
          </Typography>
        </Grid>
        <Grid item>
          <Typography fontWeight={600}>John Don </Typography>
          <Typography variant="subtitle2">+91-9589267126</Typography>
        </Grid>
      </Grid>
      <Divider />
      {/* Cart Items List */}
      <Grid my={1} container justifyContent={"space-between"}>
        <Grid item xs={4}>
          <Typography variant="subtitle2" fontWeight={"600"}>
            Items
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <Typography variant="subtitle2" fontWeight={"600"}>
            Quantity
          </Typography>
        </Grid>
        <Grid item xs={2}>
          <Typography variant="subtitle2" fontWeight={"600"}>
            Discount
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <Typography variant="subtitle2" fontWeight={"600"}>
            Net Price
          </Typography>
        </Grid>
      </Grid>
      <Divider />
      {Object.keys(items).length === 0 && (
        <Typography my={5} align="center">
          No items in cart
        </Typography>
      )}
      <Box
        sx={{
          maxHeight: 300,
          minHeight: 150,
          overflowY: "auto",
        }}
      >
        {Object.keys(items).map((key) => {
          const item = items[key];
          return (
            <Box my={1}>
              <Grid
                container
                spacing={2}
                alignItems={"center"}
                justifyContent={"space-between"}
                key={item.id}
              >
                <Grid item xs={4}>
                  <ListItemText
                    primary={item.Name}
                    secondary={`${merchantCurrency}${item.Price}`}
                  />
                </Grid>
                <Grid item xs={3}>
                  <IconButton
                    edge="end"
                    size="small"
                    onClick={() => deleteItem(item)}
                  >
                    <DeleteIcon />
                  </IconButton>
                  <IconButton
                    onClick={() => removeItem(item)}
                    edge="end"
                    size="small"
                  >
                    <RemoveIcon />
                  </IconButton>
                  {item.quantity}
                  <IconButton
                    onClick={() => addItem(item)}
                    edge="end"
                    size="small"
                  >
                    <AddIcon />
                  </IconButton>
                </Grid>
                <Grid item xs={2}>
                  <Typography> {item.Discount / 100}</Typography>
                </Grid>
                <Grid item xs={3}>
                  <Typography>
                    {" "}
                    {merchantCurrency} {round(item.quantity * item.Price, 2)}
                  </Typography>
                </Grid>
              </Grid>
            </Box>
          );
        })}
      </Box>

      <Divider />
      <Box mx={2} my={2}>
        {/* Order Summary */}
        <Grid spacing={2} container justifyContent={"space-between"}>
          <Grid item>
            {" "}
            <Typography>
              <b>Cart Total:</b>
            </Typography>{" "}
            <Typography>
              <b>Sub Total (After Discount):</b>
            </Typography>{" "}
          </Grid>
          <Grid item>
            <Typography>
              {merchantCurrency} &nbsp;
              {round(total, 2)}
            </Typography>{" "}
            <Typography>
              {merchantCurrency} &nbsp;
              {round(subTotal, 2)}
            </Typography>{" "}
          </Grid>
        </Grid>
        {/* Adjust based on your total calculation */}
      </Box>
      {/* Adjust based on your discount calculation */}
      {/* Offers Section */}
      <OffersSection />
      {/* Coupon Input */}
      <Grid my={1} container spacing={2} alignItems={"center"}>
        <Grid item xs={7}>
          <TextField
            value={couponCode}
            onChange={handleManualCouponInput}
            fullWidth
            label="Enter Coupon"
            variant="outlined"
            InputProps={{
              endAdornment:
                !applyingCoupon && !isEmpty(couponCode) && couponBlocked ? (
                  <InputAdornment position="end">
                    <CheckCircleIcon style={{ color: "green" }} />
                  </InputAdornment>
                ) : (
                  <InputAdornment position="end">
                    <CloseIcon style={{ color: "red" }} />
                  </InputAdornment>
                ),
            }}
          />
        </Grid>
        <Grid item xs={5}>
          <Button
            endIcon={applyingCoupon ? <CircularProgress size={24} /> : null}
            disabled={applyingCoupon}
            onClick={blockCoupon}
            variant="contained"
            color="primary"
          >
            {applyingCoupon ? "Checking..." : "Apply Coupon"}
          </Button>
        </Grid>
      </Grid>
      {/* Bottom Buttons */}
      <Box sx={{ mt: 2, display: "flex", gap: 1 }}>
        <Button onClick={clearCart} variant="outlined" fullWidth>
          Clear Cart
        </Button>
        <Button
          onClick={() => navigate("/pos/checkout")}
          variant="contained"
          color="primary"
          fullWidth
        >
          Checkout
        </Button>
      </Box>
    </Box>
  );
}

export default MenuCheckoutSection;
