import React, { useState, useEffect } from "react";
import { Grid, Box, Typography, Button, Divider } from "@mui/material";
import axios from "axios";
import { get } from "lodash";
import moment from "moment/moment";

import useCartStore from "../data/cartStore";
const dummy = [
  {
    couponDetails: {
      offerName: "GT2",
      couponStartDate: "2023-07-19T14:20:44",
      couponExpiryDate: "2023-09-11T12:12:44",
      couponCode: "GTC2",
      isOtpRequired: false,
    },
    couponBenefits: {
      couponBenefits: 416,
    },
  },
  {
    couponDetails: {
      offerName: "GT8",
      couponStartDate: "2023-07-19T14:47:44",
      couponExpiryDate: "2023-09-11T12:12:44",
      couponCode: "GTC8",
      isOtpRequired: false,
    },
    couponBenefits: {
      totalDiscount: 416,
    },
  },
  {
    couponDetails: {
      offerName: "GT9",
      couponStartDate: "2023-07-19T14:57:44",
      couponExpiryDate: "2023-09-11T12:12:44",
      couponCode: "GTC9",
      isOtpRequired: false,
    },
    couponBenefits: {
      totalDiscount: 416,
    },
  },
  {
    couponDetails: {
      offerName: "GT10",
      couponStartDate: "2023-07-19T14:57:44",
      couponExpiryDate: "2023-09-11T12:12:44",
      couponCode: "GTC10",
      isOtpRequired: false,
    },
    couponBenefits: {
      totalDiscount: 416,
    },
  },
  {
    couponDetails: {
      offerName: "GTV14",
      couponStartDate: "2023-07-27T10:53:52",
      couponCode: "ABCDXYZ4",
      isOtpRequired: false,
    },
    couponBenefits: {
      totalDiscount: 416,
    },
  },
  {
    couponDetails: {
      offerName: "GTV15",
      couponStartDate: "2023-07-27T11:40:19",
      couponCode: "ABCDXYZ",
      isOtpRequired: false,
    },
    couponBenefits: {
      totalDiscount: 416,
    },
  },
  {
    couponDetails: {
      offerName: "GTV16",
      couponStartDate: "2023-07-27T11:42:04",
      couponCode: "ABCDXYZA",
      isOtpRequired: false,
    },
    couponBenefits: {
      totalDiscount: 416,
    },
  },
  {
    couponDetails: {
      offerName: "HAPPYXEN TIME",
      couponStartDate: "2023-07-28T12:25:39",
      couponCode: "HAPPYXEN28",
      isOtpRequired: false,
    },
    couponBenefits: {
      totalDiscount: 416,
    },
  },
  {
    couponDetails: {
      offerName: "TEST-07082023-05",
      couponStartDate: "2023-08-07T18:08:23",
      couponCode: "TEST-07082023-05",
      isOtpRequired: false,
    },
    couponBenefits: {
      totalDiscount: 400,
    },
  },
  {
    couponDetails: {
      offerName: "TEST-07082023-07",
      couponStartDate: "2023-08-07T19:53:06",
      couponCode: "TEST-06082023-01",
      isOtpRequired: false,
    },
    couponBenefits: {
      totalDiscount: 400,
    },
  },
  {
    couponDetails: {
      offerName: "TEST-07082023-06",
      couponStartDate: "2023-08-07T18:15:36",
      couponCode: "TEST-07082023-06",
      isOtpRequired: false,
    },
    couponBenefits: {
      totalDiscount: 200,
    },
  },
];

const OffersSection = () => {
  const merchantCurrency = useCartStore((state) => state.merchantCurrency);
  const setCoupon = useCartStore((state) => state.setCoupon);
  const [offers, setOffers] = useState(dummy);
  const [loading, setLoading] = useState(false);

  const dummyData = {
    customerPhone: 9873052528,
    transactionDetails: {
      totalAmount: 2000,
      currencyCode: "INR",
      currentTimestamp: "2023-08-18T15:57:00.000",
      store: "alpha-1",
      channel: "online",
      cartItems: [
        {
          skuId: 2,
          categoryId: "1",
          categoryName: null,
          productId: null,
          productName: null,
          departmentId: "1",
          departmentName: null,
          subCategoryId: "AC",
          subCategoryName: null,
          productQuantity: 1,
          productPrice: 1000,
          productDiscount: 20,
        },
      ],
    },
  };
  let config = {
    method: "post",
    maxBodyLength: Infinity,
    url: "https://offermanagement.dev.xeno.in/api/v1/coupon/fetch",
    headers: {
      "X-Request-Source": "callerInfo",
      Authorization: "Bearer zsdbha795t",
      "Content-Type": "application/json",
    },
    data: JSON.stringify(dummyData),
  };

  useEffect(() => {
    setLoading(true);
    // axios
    //   .request(config)
    //   .then((response) => {
    //     setOffers(response.data.data.eligibleCoupons);
    //     setLoading(false);
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });
  }, []);

  return (
    <>
      <Divider />
      <Typography variant="h6">Offers</Typography>
      <Box sx={{ maxHeight: 200, overflowY: "auto" }}>
        {offers.map((offer, index) => (
          <Box
            key={index}
            sx={{
              my: 2,
              p: 2,
              bgcolor: "grey.100",
              borderRadius: 1,
            }}
          >
            <Grid
              container
              alignItems={"center"}
              justifyContent={"space-between"}
            >
              <Grid item xs={8}>
                <Typography variant="h6" gutterBottom>
                  {get(offer, "couponDetails.offerName")}
                </Typography>
              </Grid>
              <Grid item xs={4}>
                <Button onClick={()=>setCoupon(get(offer, "couponDetails.couponCode"))} fullWidth variant="outlined" size="small" color="primary">
                  <Typography
                    
                    align="center"
                    component={"span"}
                    fontWeight={600}
                    variant="subtitle2"
                    color="textSecondary"
                    gutterBottom
                  >
                    {get(offer, "couponDetails.couponCode")}
                  </Typography>
                </Button>
              </Grid>
              <Grid item>
                <Typography variant="subtitle1" gutterBottom>
                  Discount of {merchantCurrency}{" "}
                  {get(offer, "couponBenefits.totalDiscount", 0)}
                </Typography>
              </Grid>
              <Grid item>
                <Typography variant="subtitle1" gutterBottom>
                  <Typography
                    component={"span"}
                    sx={{
                      opacity: 0.4,
                    }}
                  >
                    Offer Expiry:{" "}
                  </Typography>
                  {get(offer, "couponDetails.couponExpiryDate", null)
                    ? moment(
                        get(offer, "couponDetails.couponExpiryDate")
                      ).format("Do MMM, YY | hh:m A")
                    : "Lifetime"}
                </Typography>
              </Grid>
            </Grid>

            {/* <Grid container mt={1} justifyContent={"space-between"}>
              <Button variant="contained" size="small" color="primary">
                Apply
              </Button>
            </Grid> */}
          </Box>
        ))}
      </Box>
    </>
  );
};

export default OffersSection;
