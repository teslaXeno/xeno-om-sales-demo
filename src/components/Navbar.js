import React, { useState, useEffect } from "react";
import { AppBar, Toolbar, Typography, Box, Avatar } from "@mui/material";

function NavBar() {
  const [dateTime, setDateTime] = useState(new Date());

  useEffect(() => {
    const interval = setInterval(() => {
      setDateTime(new Date());
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  return (
    <AppBar
      position="fixed"
      sx={{
        height: 80,
      }}
      //   style={{ backgroundColor: "white", color: "black" }}
    >
      <Toolbar style={{ justifyContent: "space-between" }}>
        {/* Logo */}
        <Typography variant="h6">Sample POS</Typography>

        {/* Right side boxes */}
        <Box display="flex" alignItems="center">
          {/* Date and Time */}
          <Box m={2} p={2} borderRadius={1}>
            <Typography>
              {dateTime.toLocaleDateString()} {dateTime.toLocaleTimeString()}
            </Typography>
          </Box>

          {/* Avatar + Name */}
          <Box display="flex" alignItems="center">
            <Avatar src="https://via.placeholder.com/40" alt="User avatar" />
            <Typography variant="body1" mx={2}>
              John Doe
            </Typography>
          </Box>
        </Box>
      </Toolbar>
    </AppBar>
  );
}

export default NavBar;
