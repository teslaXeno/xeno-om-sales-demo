import React, { useState, useEffect } from "react";
import { InputBase, IconButton, Paper, Box, Button } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import CloseIcon from "@mui/icons-material/Close";
import AddIcon from "@mui/icons-material/Add";
import CancelIcon from "@mui/icons-material/Cancel";
import db from "../firebase";
import { collection, doc, getDocs, where, query } from "firebase/firestore";
import useCartStore from "../data/cartStore";
function SearchBar() {
  const industry = useCartStore((state) => state.industry);
  const updateSelectedCategories = useCartStore(
    (state) => state.updateSelectedCategories
  );
  const searchTerm = useCartStore((state) => state.searchTerm);
  const updateSearchTerm = useCartStore((state) => state.updateSearchTerm);
  const updateStore = useCartStore((state) => state.updateStore);

  const [inputValue, setInputValue] = useState("");
  const [selectedCategories, setSelectedCategories] = useState([]);

  const [categories, setCategories] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      try {
        updateStore("loadingProducts", true);
        const industryRef = doc(db, "industry", industry);
        const q = query(
          collection(db, "category"),
          where("Industry", "==", industryRef)
        );
        const querySnapshot = await getDocs(q);
        let data = [];
        querySnapshot.forEach((doc) => {
          data.push({ id: doc.id, ...doc.data() });
        });
        updateStore("loadingProducts", false);
        setCategories(data.map((el) => el.Name));
        setSelectedCategories(data.map((el) => el.Name).slice(0, 1));
      } catch (err) {
        console.error("Error fetching data from Firestore", err);
      }
    };

    fetchData();
  }, [industry]);

  useEffect(() => {
    updateSelectedCategories(selectedCategories);
  }, [selectedCategories]);

  const toggleCategory = (category) => {
    if (selectedCategories.includes(category)) {
      setSelectedCategories((prevState) =>
        prevState.filter((cat) => cat !== category)
      );
    } else {
      setSelectedCategories((prevState) => [...prevState, category]);
    }
  };
  const sortedCategories = categories.sort((a, b) => {
    if (selectedCategories.includes(a)) return -1;
    if (selectedCategories.includes(b)) return 1;
    return 0;
  });

  return (
    <div>
      <Paper
        component="form"
        sx={{
          p: "2px 4px",
          display: "flex",
          alignItems: "center",
          width: "100%",
          borderRadius: 50,
        }}
      >
        <IconButton type="submit" sx={{ p: "10px" }} aria-label="search">
          <SearchIcon />
        </IconButton>
        <InputBase
          sx={{
            ml: 1,
            flex: 1,
            textAlign: "center",
          }}
          placeholder="Search products"
          value={searchTerm}
          onChange={(e) => updateSearchTerm(e.target.value)}
        />
        {inputValue && (
          <IconButton
            type="button"
            aria-label="clear"
            onClick={() => updateSearchTerm("")}
          >
            <CloseIcon />
          </IconButton>
        )}
      </Paper>
      <Box my={2} ml={1} sx={{ display: "flex", flexWrap: "wrap", gap: 1 }}>
        {sortedCategories.map((category) => (
          <Button
            sx={{
              borderRadius: 3,
            }}
            key={category}
            variant="outlined"
            color={
              selectedCategories.includes(category) ? "primary" : "default"
            }
            onClick={() => toggleCategory(category)}
            startIcon={
              selectedCategories.includes(category) ? (
                <CancelIcon />
              ) : (
                <AddIcon />
              )
            }
          >
            {category}
          </Button>
        ))}
      </Box>
    </div>
  );
}

export default SearchBar;
